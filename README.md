# My project's README

- Número: 45056
- Nome: André Filipe Gomes dos Santos
- Grupo: André Santos, André Lopes, Nuno Pulido, Sérgio Tavares, Tiago Madeira
- Link: www.codzzy.com
- Link Original: apdcdemo-162509.appspot.com
- Versão Actual: 6

Nota: 
-A funcionalidade de centrar do google maps depende da funcionalidade de geolocalização do browser.
-Os scripts de javascript estão actualmente configurados para conectar ao serviço em vez de localhost.

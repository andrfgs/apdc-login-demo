
var logoutservice = "http://apdcdemo-162509.appspot.com/rest/login/logout/";

$(window).on('load', function () {


	if (localStorage.tokenID)
	{

		$("#accountgreeting").html("Bem vindo(a), " + localStorage.tokenFullname);

		$('#logoutbtn').on('click', function(){
			$.ajax({
				url: logoutservice + localStorage.tokenID,
				success: function(response){
					console.log("Logged out!");
					localStorage.removeItem('tokenID');
					localStorage.removeItem('tokenFullname');
					window.location.replace('login.html');

				},
				error: function(response)
				{
					console.log("Got error: " + response);
				}
			});
		});

		$('#centerbtn').on('click', function(){
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) 
						{
					initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
					map.setCenter(initialLocation);
						});
			}
		});

	}
	else
	{
		window.location.replace('login.html');
	}
});

var map;

$(window).on('resize', function () {
	// (the 'map' here is the result of the created 'var map = ...' above)
	if (map)
		google.maps.event.trigger(map, "resize");
});

function initMap()
{

	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:  38.659784, lng:  -9.202765},
		zoom: 16
	});
	mapdiv = $('#map');

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			map.setCenter(initialLocation);

			var marker = new google.maps.Marker({
				position: initialLocation,
				map: map
			});
			var contentString = '<div id="content">'+
			'<h1 id="title">Você está aqui!</h1>'+
			'<p>Esta é a sua localização aproximada, com base ' +
			'nos serviços de localização do seu browser.</p>' +
			'<div id="media">'+
			'</div>';

			var infowindow = new google.maps.InfoWindow({content: contentString});

			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});
		});
	}
}
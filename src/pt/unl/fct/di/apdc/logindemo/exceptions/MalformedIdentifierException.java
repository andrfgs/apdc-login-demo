package pt.unl.fct.di.apdc.logindemo.exceptions;

public class MalformedIdentifierException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MalformedIdentifierException()
	{
		super();
	}

}

package pt.unl.fct.di.apdc.logindemo.json;

import java.util.UUID;



public class AuthToken {

	public static final long EXPIRATION = 3600000 * 2; // 1 hour * x
	
	public String username;
	public String fullname;
	public String tokenID;
	public long creationDate;
	public long expirationDate;
	
	public AuthToken(String username, String fullname)
	{
		this.username = username;
		this.fullname = fullname;
		this.tokenID = UUID.randomUUID().toString();
		this.creationDate = System.currentTimeMillis();
		this.expirationDate = this.creationDate + AuthToken.EXPIRATION;
	}
	

}

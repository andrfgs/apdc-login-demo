package pt.unl.fct.di.apdc.logindemo.json;

public class LoginData {

	
	public String identifier;
	public String password;
	
	public LoginData() {}
	
	public LoginData(String identifier, String password)
	{
		this.identifier = identifier;
		this.password = password;
	}
}

package pt.unl.fct.di.apdc.logindemo.json;

public class RegisterData {

	public String fullname;
	public String username;
	public String email;
	public int cellphone;
	public int phone;
	public String street;
	public String compl;
	public String location;
	public String zipcode;
	public int nif;
	public int citcard;
	public String password;
	public String confirmpassword;
	
	public RegisterData(){}
	
	public RegisterData(String fullname,
						String username,
						String email,
						int cellphone,
						int phone,
						String street,
						String compl,
						String location,
						String zipcode,
						int nif,
						int citcard,
						String password,
						String confirmpassword)
	{
		this.fullname = fullname;
		this.username = username;
		this.email = email;
		this.cellphone = cellphone;
		this.phone = phone;
		this.street = street;
		this.compl = compl;
		this.location = location;
		this.zipcode = zipcode;
		this.nif = nif;
		this.citcard = citcard;
		this.password = password;
		this.confirmpassword = confirmpassword;
	}
	
}

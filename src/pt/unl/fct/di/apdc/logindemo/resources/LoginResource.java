package pt.unl.fct.di.apdc.logindemo.resources;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.logindemo.exceptions.MalformedIdentifierException;
import pt.unl.fct.di.apdc.logindemo.json.AuthToken;
import pt.unl.fct.di.apdc.logindemo.json.LoginData;
import pt.unl.fct.di.apdc.logindemo.json.RegisterData;
import pt.unl.fct.di.apdc.logindemo.util.DataValidator;
import pt.unl.fct.di.apdc.logindemo.util.IdentifierResolver;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {

	public static final String USER_ENTITY = "User";
	private static final String USER_STATS_ENTITY = "UserStats";
	private static final String USER_LOG_ENTITY = "UserLog";
	
	private static final String LOGINS_STAT = "loginstats";
	private static final String LOGINS_FAILED_STAT = "loginfailedstats";
	private static final String LOGIN_IP_STAT = "loginip";
	private static final String LOGIN_HOST_STAT = "loginhost";
	private static final String LOGIN_LATLON_STAT = "loginlatlon";
	private static final String LOGIN_CITY_STAT = "logincity";
	private static final String LOGIN_COUNTRY_STAT = "logincountry";
	private static final String LOGIN_TIME_STAT = "logintime";
	private static final String LOGIN_LAST_STAT = "loginlast";
	
	private static final String FULLNAME_PROPERTY = "fullname";
	public static final String USERNAME_PROPERTY = "username";
	public static final String EMAIL_PROPERTY = "email";
	private static final String CELLPHONE_PROPERTY = "cellphone";
	private static final String PHONE_PROPERTY = "phone";
	private static final String STREET_PROPERTY = "street";
	private static final String COMPL_PROPERTY = "compl";
	private static final String LOCATION_PROPERTY = "location";
	private static final String ZIPCODE_PROPERTY = "zipcode";
	private static final String NIF_PROPERTY = "nif";
	private static final String CITCARD_PROPERTY = "citcard";
	private static final String PASSWORD_PROPERTY = "password";
	private static final String CREATION_PROPERTY = "creation";
	private static final String LAST_TOKEN_PROPERTY = "lasttoken";
	private static final String LAST_TOKEN_EXPIRARION_PROPERTY = "lasttokenexpiration";

	private static final Gson gson = new Gson();
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public LoginResource() {
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response doLogin(LoginData data,
			@Context HttpServletRequest request,
			@Context HttpHeaders headers) {
		LOG.fine("Received login attempt with: " + data.identifier);

		Transaction txn = datastore.beginTransaction();
		try
		{
			int id = IdentifierResolver.tryResolve(data.identifier, datastore);

			Key userKey = KeyFactory.createKey(USER_ENTITY, id);
			Entity user = datastore.get(userKey);
			String username = user.getProperty(USERNAME_PROPERTY).toString();

			// Obtain user login statistics
			Query statsQ = new Query(USER_STATS_ENTITY).setAncestor(userKey);
			List<Entity> results = datastore.prepare(statsQ).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty())
			{
				// Initialize stats for the first time
				ustats = new Entity(USER_STATS_ENTITY, user.getKey());
				ustats.setProperty(LOGINS_STAT, 0L);
				ustats.setProperty(LOGINS_FAILED_STAT, 0L);
			}
			else
			{
				ustats = results.get(0);
			}
			
			// Validate password
			String hashedpass = user.getProperty(PASSWORD_PROPERTY).toString();
			if (safeEquals(hashedpass, DigestUtils.sha512Hex(data.password)))
			{
				// Valid password
				
				// Construct logs
				Entity log = new Entity(USER_LOG_ENTITY, user.getKey());
				log.setProperty(LOGIN_IP_STAT, request.getRemoteAddr());
				log.setProperty(LOGIN_HOST_STAT, request.getRemoteHost());
				log.setProperty(LOGIN_LATLON_STAT, headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty(LOGIN_CITY_STAT, headers.getHeaderString("X-AppEngine-City"));
				log.setProperty(LOGIN_COUNTRY_STAT, headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty(LOGIN_TIME_STAT, new Date());
				
				// Update user statistics
				ustats.setProperty(LOGINS_STAT, 1L + (long)ustats.getProperty(LOGINS_STAT));
				ustats.setProperty(LOGINS_FAILED_STAT, 0L);
				ustats.setProperty(LOGIN_LAST_STAT, new Date());
				
				// Batch opeation
				List<Entity> logs = Arrays.asList(log, ustats);
				datastore.put(txn, logs);
				
				// Return token
				AuthToken token = new AuthToken(username, 
						user.getProperty(FULLNAME_PROPERTY).toString());
				user.setProperty(LAST_TOKEN_PROPERTY, token.tokenID);
				user.setProperty(LAST_TOKEN_EXPIRARION_PROPERTY, token.expirationDate);
				datastore.put(txn, user);
				txn.commit();
				
				LOG.info("User '" + username + "' logged in successfully.");
				return Response.ok(gson.toJson(token)).build();
			}
			else
			{
				// Wrong Password
				ustats.setProperty(LOGINS_FAILED_STAT, 1L + (long)ustats.getProperty(LOGINS_FAILED_STAT));
				datastore.put(txn, ustats);
				txn.commit();
				
				LOG.warning("Wrong password for username: " + username);
				return Response.status(Status.FORBIDDEN).entity("Credenciais inválidas!").build(); 
			}
			
		}
		catch (MalformedIdentifierException ex)
		{
			LOG.warning("Invalid identifier for login: " + data.identifier);
			return Response.status(Status.FORBIDDEN).entity("Credenciais inválidas!").build();
		}
		catch (Exception ex)
		{
			return Response.status(Status.FORBIDDEN).entity("Credenciais inválidas!").build();
		}
		finally
		{
			if (txn.isActive())
			{
				txn.rollback();
				//return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

		
	}

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(RegisterData data) {
		LOG.fine("Received register attempt from: " + data.username);

		Response r;
		DataValidator av = new DataValidator(data);

		if (!av.isRegistryValid())
		{
			LOG.fine("Bad login from: " + data.username + ". Error: " + av.getErrorMessages());
			return Response.status(Status.BAD_REQUEST).entity(av.getErrorMessages()).build();
		}

		String databaseMsg = "";
		if (usernameExists(data.username))
			databaseMsg += "O Username introduzido já está registado! ";
		if (emailExists(data.email))
			databaseMsg += "O Email introduzido já está registado! ";
		
		if (!databaseMsg.equals(""))
			return Response.status(Status.BAD_REQUEST).entity(databaseMsg).build();
		
		Transaction txn = datastore.beginTransaction();
		try
		{

			Key userKey = KeyFactory.createKey(USER_ENTITY, data.nif);
			Entity user = datastore.get(userKey);
			databaseMsg += "O NIF introduzido já está registado! ";

			txn.rollback();
		}
		catch (EntityNotFoundException ex)
		{

			Entity user = new Entity(USER_ENTITY, data.nif);
			user.setUnindexedProperty(FULLNAME_PROPERTY, data.fullname);
			user.setProperty(USERNAME_PROPERTY, data.username);
			user.setProperty(EMAIL_PROPERTY, data.email);
			user.setUnindexedProperty(CELLPHONE_PROPERTY, data.cellphone);
			user.setUnindexedProperty(PHONE_PROPERTY, data.phone);
			user.setUnindexedProperty(STREET_PROPERTY, data.street);
			user.setUnindexedProperty(COMPL_PROPERTY, data.compl);
			user.setProperty(LOCATION_PROPERTY, data.location);
			user.setProperty(ZIPCODE_PROPERTY, data.zipcode);
			user.setUnindexedProperty(NIF_PROPERTY, data.nif);
			user.setUnindexedProperty(CITCARD_PROPERTY, data.citcard);
			user.setUnindexedProperty(PASSWORD_PROPERTY, DigestUtils.sha512Hex(data.password));
			user.setUnindexedProperty(CREATION_PROPERTY, new Date());
			user.setProperty(LAST_TOKEN_PROPERTY, "");
			user.setProperty(LAST_TOKEN_EXPIRARION_PROPERTY, 0L);
			datastore.put(txn, user);

			txn.commit();
			return Response.ok(gson.toJson("Registado com Sucesso!")).build();
		}
		finally
		{
			if (txn.isActive())
				txn.rollback();
		}

		LOG.fine("Error while registering: " + databaseMsg);
		return Response.status(Status.BAD_REQUEST).entity(databaseMsg).build();
	}
	
	@GET
	@Path("/logout/{tokenid}")
	public Response logout(@PathParam("tokenid") String tokenId) {
		
		Transaction txn = null;
		try
		{
			LOG.fine("Attempt to logout token id: " + tokenId);
			
			// Check if token is valid
			Entity user = getUserFromTokenId(tokenId);
			
			if (user == null)
				throw new Exception();
			
			txn = datastore.beginTransaction();
			user.setProperty(LAST_TOKEN_PROPERTY, "");
			datastore.put(txn, user);
			txn.commit();
			
			LOG.fine("User '" + user.getProperty(USERNAME_PROPERTY) + "' has logged out successfully!");
			return Response.ok().entity(gson.toJson("Successfully logged out!")).build();
		}
		catch (Exception ex)
		{
			if (txn != null)
				txn.rollback();
			return Response.status(Status.FORBIDDEN).build();
		}
		finally
		{
			if (txn != null)
				if (txn.isActive())
					txn.rollback();
		}
	}
	
	public Entity getUserFromTokenId(String id)
	{
		Filter keyFilter = new FilterPredicate(LAST_TOKEN_PROPERTY, Query.FilterOperator.EQUAL, id);
		Query q = new Query(LoginResource.USER_ENTITY).setFilter(keyFilter);

		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		
		if (results.size() == 1)
			return results.get(0);
		else
			return null;
	}

	private static boolean usernameExists(String username)
	{
		Filter keyFilter = new FilterPredicate(USERNAME_PROPERTY, Query.FilterOperator.EQUAL, username);
		Query q = new Query(LoginResource.USER_ENTITY).setFilter(keyFilter);

		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());

		return (results.size() > 0);
	}

	private static boolean emailExists(String email)
	{
		Filter keyFilter = new FilterPredicate(EMAIL_PROPERTY, Query.FilterOperator.EQUAL, email);
		Query q = new Query(LoginResource.USER_ENTITY).setFilter(keyFilter);

		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());

		return (results.size() > 0);
	}

	// Compares two strings safely without returning immediately
	// when a char doesn't match. This will prevent password time
	// based attacks.
	public static boolean safeEquals(String a, String b)
	{
		boolean equals = true;

		char[] aarray = a.toCharArray();
		char[] barray = b.toCharArray();
		for (int i=0; i < barray.length; i++)
			if (aarray[i] != barray[i])
				equals = false;

		return equals;
	}

}


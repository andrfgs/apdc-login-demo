package pt.unl.fct.di.apdc.logindemo.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import pt.unl.fct.di.apdc.logindemo.json.RegisterData;

public class DataValidator {
	private static final int MAX_USERNAME_LENGTH = 20;
	private static final int MIN_USERNAME_LENGTH = 4;
	private static final int MAX_PASSWORD_LENGTH = 20;
	private static final int MIN_PASSWORD_LENGTH = 7;
	private static final String PASSWORD_SYMBOLS = "@-_,;~´`.*/=+%#&!?$£€§";
	private static final int[] VALID_NIF_PREFIXES = { 1, 2, 5, 6, 8 };

	private RegisterData data;
	private String errorMessages = "";
	
	public DataValidator(RegisterData data)
	{
		this.data = data;
		this.errorMessages = validateRegistry(this.data);
	}
	
	public boolean isRegistryValid()
	{
		return this.errorMessages.equals("");
	}
	
	public String getErrorMessages()
	{
		return this.errorMessages;
	}
	
	private static String validateRegistry(RegisterData data) {
		String message = "";

		if (data.fullname.trim().equals(""))
			message += "O Nome Completo não pode estar vazio. ";

		if (!isUsernameValid(data.username))
			message += "O username deve ter de " + MIN_USERNAME_LENGTH + 
			" a " + MAX_USERNAME_LENGTH + " caracteres. ";

		if (!isEmailValid(data.email))
			message += "Email inválido. ";

		if (!isCellphoneValid(data.cellphone))
			message += "Número de telemóvel inválido. ";

		if (!isphoneValid(data.phone))
			message += "Número de telefone inválido. ";

		if (data.street.trim().equals(""))
			message += "A Rua não pode estar vazia. ";

		if (data.location.trim().equals(""))
			message += "A Localidade não pode estar vazia. ";

		if (!isZipCodeValid(data.zipcode))
			message += "O código postal inserido é inválido. ";

		if (!isNifValid(data.nif))
			message += "NIF inválido. ";

		if (!isPasswordValid(data.password))
			message += "A palavra-passe deve ter de " + MIN_PASSWORD_LENGTH + 
			" a " + MAX_PASSWORD_LENGTH + " caracteres. Deve ter números, "
			+ "letras e símbolos. Os seguintes símbolos são aceites: " + 
			PASSWORD_SYMBOLS + ". ";

		if(!data.password.equals(data.confirmpassword))
			message += "A palavra-passe deve ser igual à confirmação. ";

		return message;
	}


	public static boolean isEmailValid(String email) {
		boolean result = true;

		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public static boolean isNifValid(int nif) {
		if (nif < 000000000 && nif > 999999999)
			return false;

		int prefix = nif / 100000000;

		// Validate prefix
		boolean validPrefix = false;
		for (int i : VALID_NIF_PREFIXES)
			if (prefix == i)
				validPrefix = true;
		if (!validPrefix)
			return false;

		// Obtain control digit
		String nifStr = Integer.toString(nif);
		int expectedControl = 0;
		for (int i = 0; i < 8; i++)
			expectedControl += (9 - i) * Integer.parseInt(nifStr.substring(i, i + 1));
		expectedControl %= 11;
		if (expectedControl < 2)
			expectedControl = 0;
		else
			expectedControl = 11 - expectedControl;

		// Validate control digit
		int control = nif % 10;
		if (control != expectedControl)
			return false;

		return true;
	}

	public static boolean isUsernameValid(String username)
	{
		if (username.length() < MIN_USERNAME_LENGTH ||
				username.length() > MAX_USERNAME_LENGTH)
			return false;
		return true;
	}

	private static boolean isPasswordValid(String password)
	{
		if (password.length() < MIN_PASSWORD_LENGTH ||
				password.length() > MAX_PASSWORD_LENGTH)
			return false;

		String text = password.replaceAll("[^A-Za-z]+","");
		String numbers = password.replaceAll("\\D+","");

		String symRegex = "[^" + PASSWORD_SYMBOLS + "]+";
		String symbols = password.replaceAll(symRegex, "");

		if (text.length() == 0 || numbers.length() == 0 || symbols.length() == 0)
			return false;

		return true;
	}

	private static boolean isCellphoneValid(int cell)
	{
		return  Integer.toString(cell).matches("[9]{1}[1236]{1}[0-9]{7}");
	}

	private static boolean isphoneValid(int cell)
	{
		if (cell == 0)
			return true;
		return  Integer.toString(cell).matches("[2]{1}[0-9]{8}");
	}

	private static boolean isZipCodeValid(String zipcode) {
		String[] zipparts = zipcode.split("-");

		if (zipparts.length != 2)
			return false;

		try
		{
			Integer.parseInt(zipparts[0]);
			Integer.parseInt(zipparts[1]);
		}
		catch (NumberFormatException ex)
		{ return false; }

		return true;
	}
}

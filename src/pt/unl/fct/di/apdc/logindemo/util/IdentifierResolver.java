package pt.unl.fct.di.apdc.logindemo.util;


import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import pt.unl.fct.di.apdc.logindemo.exceptions.MalformedIdentifierException;
import pt.unl.fct.di.apdc.logindemo.resources.LoginResource;

public class IdentifierResolver {

	private static final String UNIQUE_PROPERTY = "nif";


	public static int tryResolve(String identifier, DatastoreService datastore) throws MalformedIdentifierException
	{
		// Check if its nif
		try
		{
			int id = Integer.parseInt(identifier);
			if (DataValidator.isNifValid(id))
				return id;
		} catch (Exception ex) {}


		Entity e;

		// Query for mail
		e = queryElement(identifier, LoginResource.EMAIL_PROPERTY, datastore);
		if (e != null)
		{
			try
			{
				return (int)(long) e.getProperty(UNIQUE_PROPERTY);
			}
			catch (Exception ex) {}
		}

		// Query for username
		e = queryElement(identifier, LoginResource.USERNAME_PROPERTY, datastore);
		if (e != null)
		{
			try
			{
				return (int)(long) e.getProperty(UNIQUE_PROPERTY);
			}
			catch (Exception ex) {}
		}

		throw new MalformedIdentifierException();
	}

	private static Entity queryElement(String identifier, String property, DatastoreService datastore)
	{
		Filter keyFilter = new FilterPredicate(property, Query.FilterOperator.EQUAL, identifier);
		Query q = new Query(LoginResource.USER_ENTITY).setFilter(keyFilter);

		List<Entity> results = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		
		if (results.size() == 1)
			return results.get(0);

		return null;
	}
}

//var loginservice = "apdcdemo-162509.appspot.com/rest/login";

var errormessage = "Os seguintes erros ocorreram: ";
var loginservice = "http://apdcdemo-162509.appspot.com/rest/login";
var registerservice = "http://apdcdemo-162509.appspot.com/rest/login/register";

$(window).on('load', function () {
	if (localStorage.tokenID)
	{
		window.location.replace('accountpage.html');
	}
	else
	{

		$("#loginform").on('submit', attemptLogin);
		$("#registerform").on('submit', attemptRegister);

		console.log('Login services started.');
	}

});

function attemptLogin(event)
{
	var data = $('form[name="loginform"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: loginservice,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {

				$('#loginerrorlabel').hide();
				console.log("Got token: " + response.tokenID);
				
				// Store token id for later use in localStorage
				localStorage.setItem('tokenID', response.tokenID);
				localStorage.setItem('tokenFullname', response.fullname);
				
				// Redirect to logged in page
				window.location.replace('accountpage.html');
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			console.log("Got error status " + response.status + " " + response.responseText);
			$('#loginerrorlabel').show();
			$('#loginerrorlabel').html(errormessage + response.responseText);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
}

function attemptRegister(event)
{
	var data = $('form[name="registerform"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: registerservice,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				console.log(response);

				$('#registererrorlabel').hide();
				$('#registersuccesslabel').show();
				$('#registersuccesslabel').html(response);
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			console.log("Got error status " + response.status + " " + response.responseText);
			$('#registersuccesslabel').hide();
			$('#registererrorlabel').show();
			$('#registererrorlabel').html(errormessage + response.responseText);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
}
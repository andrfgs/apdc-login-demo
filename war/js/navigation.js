var tabs = [
	["img=miniLogo.png", "index.html"], 
	["Not&iacutecias", "news.html"], 
	["Quem Somos?", "whoarewe.html"], 
	["Login", "login.html"],
	["$bar=fullname", "accountpage.html"]];

window.onload = function init()
{
	var navbar = document.getElementById("navbar");
	var pagename = location.pathname;
	pagename = pagename.substring(pagename.lastIndexOf("/")+1);

	for (var t = 0; t < tabs.length; t++)
	{
		if (localStorage.tokenID)
		{
			if (tabs[t][0] == 'Login')
				continue;
			else if (tabs[t][0] == '$bar=fullname')
				tabs[t][0] = localStorage.tokenFullname;
		}
		else if (tabs[t][0] == '$bar=fullname')
			continue;

		var li = document.createElement("li");
		var a = document.createElement("a");
		a.setAttribute('href', tabs[t][1]);

		var classes = "";
		classes += "navlia ";

		if (tabs[t][1] == pagename)
			classes += "active ";

		if (startsWith(tabs[t][0], "img="))
		{
			var img = document.createElement("img");
			img.setAttribute('width', '30px');
			img.setAttribute('src', "img/" + tabs[t][0].replace("img=", ""));

			classes += "imga ";

			a.appendChild(img);
		}

		else
			a.innerHTML = tabs[t][0];

		a.setAttribute('class', classes);
		li.setAttribute('class', 'navli')
		li.appendChild(a);
		navbar.appendChild(li);
	}
}

function openTab(event, tabName) {
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(tabName).style.display = "block";
	event.currentTarget.className += " active";
}

//IE Does not include string.startsWith()
function startsWith(str, value)
{
	var starts = true;

	for (var c = 0; c < value.length; c++)
		if (str.charAt(c) != value.charAt(c))
		{
			starts = false;
			break;
		}

	return starts;
}
